##Previous

- Read conclusion.txt

##Install

```
bash install.sh
```
##RUN
```
docker-compose exec api bash simulation.sh
```

##After

- Go http://127.0.0.1:4000/v1/report

- Read part2.txt