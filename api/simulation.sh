#!/usr/bin/env bash

echo 'start simulation...'

curl http://127.0.0.1/v1/start

curl http://127.0.0.1/v1/execute?sequence=0 &
curl http://127.0.0.1/v1/execute?sequence=1 &
curl http://127.0.0.1/v1/execute?sequence=2 &
curl http://127.0.0.1/v1/execute?sequence=3 &
curl http://127.0.0.1/v1/execute?sequence=4 &
curl http://127.0.0.1/v1/execute?sequence=5

echo 'Go to http://127.0.0.1:4000/v1/report to see your report'