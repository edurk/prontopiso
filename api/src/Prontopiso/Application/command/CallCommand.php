<?php

namespace App\Prontopiso\Application\command;


use App\Prontopiso\Application\service\cache\CacheService;
use App\Prontopiso\Domain\building\Building;
use App\Prontopiso\Domain\call\Call;
use App\Prontopiso\Domain\elevator\Elevator;
use Psr\Log\LoggerInterface;

class CallCommand
{
    /**
     * @var CacheService
     */
    private $cacheService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var int
     */
    private $seconds = 1;

    /**
     * @var int
     */
    private $executedFrequency;

    /**
     * @var Building $building
     */
    private $building;

    /**
     * CallElevatorCommand constructor.
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(LoggerInterface $logger, CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
        $this->logger = $logger;
    }

    /**
     * @param Call $sequence
     * @return mixed
     * @throws \Exception
     */
    public function handle(Call $sequence)
    {

        /** @var Building $building * */
        $this->building = $this->cacheService->has('building') ? unserialize($this->cacheService->get('building')) : false;

        if (!$this->building) {
            throw new \Exception('building is not initialized');
        }

        /** @comment frequency count */
        $this->executedFrequency = $sequence->timeStart();

        /**
         * @comment start count day since 00.00h
         */
        while ($this->seconds <= $this->getDayInSeconds()) {

            $this->addMinuteStatistics();

            if ($this->seconds >= $sequence->timeStart() && $this->seconds <= $sequence->timeEnd()) {

                if ($this->seconds === $this->executedFrequency) {

                    $available = [];
                    $selectedElevators = [];

                    /** @comment calculate elevators status and which elevator is nearest from request */
                    /**@var Elevator $elevator * */
                    foreach ($sequence->positionStart() as $positionOnDemand) {
                        foreach ($this->building->elevators() as $elevator) {
                            if ($elevator->state() === Elevator::STATE_AVAILABLE) {
                                $available[] = $elevator;
                            }
                        }
                        if (empty($available)) {
                            continue;
                        }

                        $selectedElevator = false;
                        foreach ($available as $elevator) {

                            $closest =($positionOnDemand >= $elevator->position())
                                ? $positionOnDemand / $elevator->position()
                                : $elevator->position() / $positionOnDemand;
                            /**
                             * @var Elevator $selectedElevator
                             */
                            if (!$selectedElevator) {
                                $selectedElevator = $elevator;
                            } elseif ($closest < $selectedElevator->position()) {
                                $selectedElevator = $elevator;
                            }
                        }

                        /**@comment update elevators status and positions **/
                        $selectedElevator->setState(Elevator::STATE_PENDING);
                        $elevator->setPosition($positionOnDemand);
                        $posEnd = $sequence->positionEnd();
                        $selectedElevator->addTravel($positionOnDemand, reset($posEnd));
                        $selectedElevators[] = $selectedElevator;
                    }

                    /** @comment update cache with new changes */
                    $this->cacheService->set('building', serialize($this->building));

                    /** @comment wait real elevator travel in seconds*/
                    sleep($this->building->elevatorsSpeed() * 2);

                    foreach ($selectedElevators as $elevator) {
                        /** @comment when elevator finish travel, update another time status and positions */
                        $elevator->setState(Elevator::STATE_AVAILABLE);
                        $elevator->setPosition(reset($posEnd));
                        $this->cacheService->set('elevator-' . $elevator->id() . '-travel', $elevator->travel());
                    }

                    /** @comment update cache with new changes */
                    $this->cacheService->set('building', serialize($this->building));

                    /** @comment increase frequency counter */
                    $this->executedFrequency = $this->executedFrequency + $sequence->frequency();
                }
            }

            $this->seconds++;
        }

        return true;

    }

    /**
     * @return float|int
     */
    private function getDayInSeconds()
    {
        return 24 * 3600;
    }


    private function addMinuteStatistics()
    {
        $date = new \DateTime("@$this->seconds");

        if ($date->format('s') == '00') {
            $report = json_decode($this->cacheService->get('report'), true);
            $elevatorsPositions = [];

            /**@var Elevator $elevator * */
            foreach ($this->building->elevators() as $elevator) {
                $elevatorsPositions[$elevator->id()] = $elevator->position();
            }

            $report[] = ['date' => $date->format('H:i:s'), 'elevators' => $elevatorsPositions];
            $this->cacheService->set('report', json_encode($report));
        }
    }
}