<?php

namespace App\Prontopiso\Application\command;


use App\Prontopiso\Application\service\cache\CacheService;
use Psr\Log\LoggerInterface;

class GetReportCommand
{
    /**
     * @var CacheService
     */
    private $cacheService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CallElevatorCommand constructor.
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(LoggerInterface $logger, CacheService $cacheService){
        $this->cacheService = $cacheService;
        $this->logger = $logger;
    }
    
    public function handle(){

        
        $travel= [
            $this->cacheService->get('elevator-0-travel'),
            $this->cacheService->get('elevator-1-travel'),
            $this->cacheService->get('elevator-2-travel')
        ];
            
        return [
            'report' => json_decode($this->cacheService->get('report'), true),
            'travel' => $travel
        ];
        
    }
    

}