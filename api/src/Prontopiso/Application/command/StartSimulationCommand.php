<?php

namespace App\Prontopiso\Application\command;


use App\Prontopiso\Application\service\cache\CacheService;
use App\Prontopiso\Domain\building\Building;
use App\Prontopiso\Domain\elevator\Elevator;
use App\Prontopiso\Domain\floor\Floor;
use Psr\Log\LoggerInterface;

class StartSimulationCommand
{
    /**
     * @var CacheService
     */
    private $cacheService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CallElevatorCommand constructor.
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(LoggerInterface $logger, CacheService $cacheService){
        $this->cacheService = $cacheService;
        $this->logger = $logger;
    }

    /**
     * @throws \Throwable
     */
    public function handle(){

        $building = new Building();

        $building->addFloor(new Floor(1));
        $building->addFloor(new Floor(2));
        $building->addFloor(new Floor(3));
        $building->addFloor(new Floor(4));

        $building->addElevator(new Elevator(1,1, Elevator::STATE_AVAILABLE));
        $building->addElevator(new Elevator(2,1, Elevator::STATE_AVAILABLE));
        $building->addElevator(new Elevator(3,1, Elevator::STATE_AVAILABLE));
        

        try {
            $this->cacheService->set('building', serialize($building));
            $this->cacheService->set('report', json_encode([]));
        }catch(\Throwable $error){
            throw $error;
        }

        return true;

    }

}