<?php

namespace App\Prontopiso\Application\service\cache;


interface CacheService
{
    /**
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param $key
     * @param $value
     * @param null $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl = null);

    /**
     * @param $key
     * @return mixed
     */
    public function delete($key);

    /**
     * @param $key
     * @return mixed
     */
    public function has($key);

}