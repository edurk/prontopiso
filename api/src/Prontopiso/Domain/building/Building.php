<?php

namespace App\Prontopiso\Domain\building;

use App\Prontopiso\Domain\elevator\Elevator;
use App\Prontopiso\Domain\floor\Floor;

class Building
{
    private $floors = [];
    
    private $elevators = [];
    
    const SPEED=0;
    
    /**
     * @return mixed
     */
    public function floors()
    {
        return $this->floors;
    }
    /**
     * @return mixed
     */
    public function elevators()
    {
        return $this->elevators;
    }
    
    public function addFloor(Floor $floor){
        
        $this->floors[] = $floor;
        
    }

    public function addElevator(Elevator $elevator){

        $this->elevators[] = $elevator;

    }
    
    public function elevatorsSpeed(){
        return self::SPEED;
    }

    /**
     * @return array
     */
    public function toArray(){
        
        $floors=$elevators=[];
        
        foreach ($this->floors as $floor){
            $floors[]=$floor->serialize();
        }
            
        foreach ($this->elevators as $elevator){
            $elevators[]=$elevator->serialize();
        }
        
        return [
            'floors' => $floors,
            'elevators' => $elevators
        ];
        
    }
}