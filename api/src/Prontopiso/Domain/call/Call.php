<?php

namespace App\Prontopiso\Domain\call;

class Call
{


    private $frequency;
    private $timeStart;
    private $timeEnd;
    private $positionStart;
    private $positionEnd;

    public function __construct($frequency, $timeStart, $timeEnd, array $positionStart, array $positionEnd)
    {
        
        $this->frequency = $frequency;
        $this->timeStart = $timeStart;
        $this->timeEnd = $timeEnd;
        $this->positionStart = $positionStart;
        $this->positionEnd = $positionEnd;
    }

    

    /**
     * @return mixed
     */
    public function frequency()
    {
        return $this->frequency;
    }

    /**
     * @return mixed
     */
    public function timeStart()
    {
        return $this->timeStart;
    }

    /**
     * @return mixed
     */
    public function timeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * @return mixed
     */
    public function positionStart()
    {
        return $this->positionStart;
    }

    /**
     * @return mixed
     */
    public function positionEnd()
    {
        return $this->positionEnd;
    }

}