<?php

namespace App\Prontopiso\Domain\elevator;

class Elevator
{
    const STATE_AVAILABLE = 'available';
    const STATE_PENDING = 'pending';
    /**
     * @var int
     */
    private $state;
    private $position;
    private $id;
    private $travel = 0;

    /**
     * Elevator constructor.
     * @param int $initialPosition
     * @param $state
     */
    public function __construct($id, $initialPosition = 0, $state)
    {
        $this->position = $initialPosition;
        $this->state = $state;
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function state()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function position()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }
    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }
    
    public function addTravel($start, $end){
        $this->travel = $this->travel + (($start > $end) ? $start - $end : $end - $start);
    }

    /**
     * @return int
     */
    public function travel(): int
    {
        return $this->travel;
    }
    
    public function toArray(){
        return [
            'state' => $this->state(),
            'position' => $this->position()
        ];
    }

    


}