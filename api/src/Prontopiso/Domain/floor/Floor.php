<?php

namespace App\Prontopiso\Domain\floor;


class Floor
{

    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
            'id' => $this->id()
        ];
    }

}