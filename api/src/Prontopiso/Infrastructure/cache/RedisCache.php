<?php

namespace App\Prontopiso\Infrastructure\cache;


use App\Prontopiso\Application\service\cache\CacheService;
use Predis\Client;


class RedisCache implements CacheService
{
    const TTL_MINUTE=60;
    const TTL_HOUR=3660;
    const TTL_DAY=86400;
    /**
     * @var Client
     */
    private $client;


    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $key
     * @return mixed|string
     */
    public function get($key)
    {
        return $this->client->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param null $ttl
     * @return bool|void
     */
    public function set($key, $value, $ttl = null)
    {
        if($ttl > 0) {
            $this->client->setex($key, $ttl, $value);
        }else{
            $this->client->set($key, $value);
        }
    }
    

    /**
     * @param $key
     */
    public function delete($key)
    {
        $this->client->del(array($key));
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        $object=$this->client->get($key);

        if(!$object) {
            return false;
        }

        return true;
    }
    
}