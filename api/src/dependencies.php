<?php
// DIC configuration

use App\Controllers\DefaultController;
use App\Prontopiso\Application\command\CallCommand;
use App\Prontopiso\Application\command\GetReportCommand;
use App\Prontopiso\Application\command\StartSimulationCommand;
use App\Prontopiso\Application\service\cache\CacheService;
use App\Prontopiso\Infrastructure\cache\RedisCache;
use Predis\Client as Redis;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $stream = new Monolog\Handler\StreamHandler($settings['path'], $settings['level']);
    //$logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $rotatingHandler = new Monolog\Handler\RotatingFileHandler($settings['path'], 10, Monolog\Logger::DEBUG);
    $logger->pushHandler($rotatingHandler);
    return $logger;
};

$container['redis'] = function ($c) {
  return new Redis('tcp://cache:6379');
};

$container[CacheService::class] = function ($container) {
    return new RedisCache(
        $container->get('redis')
    );
};

$container[StartSimulationCommand::class] = function ($container) {
    return new StartSimulationCommand(
        $container->get('logger'),
        $container->get(CacheService::class)
    );
};

$container[CallCommand::class] = function ($container) {
    return new CallCommand(
        $container->get('logger'),
        $container->get(CacheService::class)
    );
};

$container[GetReportCommand::class] = function ($container) {
    return new GetReportCommand(
        $container->get('logger'),
        $container->get(CacheService::class)
    );
};


$container['view'] = function ($container) {

    $view = new \Slim\Views\Twig( $container->get('settings')['renderer']['template_path']);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$container[DefaultController::class] = function ($container) {
    return new DefaultController(
        $container->get('view'),
        $container->get('logger'),
        $container->get(StartSimulationCommand::class),
        $container->get(CallCommand::class),
        $container->get(GetReportCommand::class)
    );
};

$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        // retrieve logger from $container here and log the error
        $logger = $container->get('logger');
        $logger->error($exception->getMessage().' '.$exception->getTraceAsString());
        $response->getBody()->rewind();
        return $response->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->withJson(['error' => $exception->getMessage()]);
    };
};

$container['phpErrorHandler'] = function ($container) {
    return $container['errorHandler'];
};
