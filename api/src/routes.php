<?php

use App\Controllers\DefaultController;

// Routes
$app->group('/v1', function () {
    $this->get('', DefaultController::class . ':index');
    $this->get('/start', DefaultController::class . ':start');
    $this->get('/report', DefaultController::class . ':getReport');
    $this->get('/execute', DefaultController::class . ':executeSimulator');
});



