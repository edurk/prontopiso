<?php
namespace App\Controllers;

use Psr\Log\LoggerInterface;

class Controller
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

}