<?php

namespace App\Controllers;

use App\Prontopiso\Application\command\CallCommand;
use App\Prontopiso\Application\command\GetReportCommand;
use App\Prontopiso\Application\command\StartSimulationCommand;
use App\Prontopiso\Domain\call\Call;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Response;
use Slim\Views\Twig;

class DefaultController extends Controller
{
    /**
     * @var StartSimulationCommand
     */
    private $cmd;
    /**
     * @var CallCommand
     */
    private $callCommand;
    /**
     * @var GetReportCommand
     */
    private $getReportCommand;
    /**
     * @var Twig
     */
    private $view;

    public function __construct(
        Twig $view,
        LoggerInterface $logger,
        StartSimulationCommand $cmd,
        CallCommand $callCommand,
        GetReportCommand $getReportCommand
    )
    {
        parent::__construct($logger);
        $this->cmd = $cmd;
        $this->callCommand = $callCommand;
        $this->getReportCommand = $getReportCommand;
        $this->view = $view;
    }


    public function index(ServerRequestInterface $request, Response $response){
        return $this->view->render($response, 'report.html', ['product' => 12]);
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @throws \Throwable
     */
    public function start(ServerRequestInterface $request, Response $response)
    {
        try {
            $this->cmd->handle();
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage(), ['trace' => $error->getTraceAsString()]);
            throw new \Exception($error->getMessage());
        }

        return $response->write('');

    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @throws \Throwable
     */
    public function executeSimulator(ServerRequestInterface $request, Response $response)
    {

        $sequenceNumber = $request->getParam('sequence');

        $sequences = [
            [
                'frequency' => '300',
                'timeStart' => 9 * 3600,
                'timeEnd' => 11 * 3600,
                'positionStart' => [1],
                'positionEnd' => [3]
            ],
            [
                'frequency' => '600',
                'timeStart' => 9 * 3600,
                'timeEnd' => 10 * 3600,
                'positionStart' => [1],
                'positionEnd' => [2]
            ],
            [
                'frequency' => '1200',
                'timeStart' => 11 * 3600,
                'timeEnd' => 18.2 * 3600,
                'positionStart' => [1],
                'positionEnd' => [2, 3, 4]
            ],
            [
                'frequency' => '240',
                'timeStart' => 14 * 3600,
                'timeEnd' => 15 * 3600,
                'positionStart' => [2, 3, 4],
                'positionEnd' => [1]
            ],
            [
                'frequency' => '420',
                'timeStart' => 15 * 3600,
                'timeEnd' => 16 * 3600,
                'positionStart' => [3, 4],
                'positionEnd' => [1]
            ],
            [
                'frequency' => '180',
                'timeStart' => 18 * 3600,
                'timeEnd' => 20 * 3600,
                'positionStart' => [2, 3, 4],
                'positionEnd' => [1]
            ]
        ];

        try {
            $this->callCommand->handle(
                new Call($sequences[$sequenceNumber]['frequency'],
                    $sequences[$sequenceNumber]['timeStart'],
                    $sequences[$sequenceNumber]['timeEnd'],
                    $sequences[$sequenceNumber]['positionStart'],
                    $sequences[$sequenceNumber]['positionEnd'])
            );
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage(), ['trace' => $error->getTraceAsString()]);
            throw $error;
        }

        return $response->write('');

    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Throwable
     */
    public function getReport(ServerRequestInterface $request, Response $response)
    {

        try {
            $reports = $this->getReportCommand->handle();
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage(), ['trace' => $error->getTraceAsString()]);
            throw $error;
        }

        $statistics = [];

        foreach ($reports['report'] as $key => $report) {

            $init = new \DateTime($report['date']);

            if ( (int)$init->format('H') >= 9 && (int)$init->format('H') < 20) {
                $statistics[]=$report;
            }
        }

        return $this->view->render($response, 'report.html', ['reports' => $statistics]);

    }
}