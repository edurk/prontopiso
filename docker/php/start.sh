#!/bin/bash

readonly HOST_USERID=$(stat -c '%u' "$PWD"/composer.json)
usermod -u $HOST_USERID www-data && \

printf "syntax on\nset number" > /root/.vimrc && \

readonly PHP_VERSION=7.1
readonly SCRIPT_DIR=$(pwd)/$(dirname "$0")
sed -i.default -e 's/^listen = .*$/listen = 127.0.0.1:9000/g' /etc/php/"$PHP_VERSION"/fpm/pool.d/www.conf
ln -sf /docker/php/xdebug.ini /etc/php/"$PHP_VERSION"/mods-available/xdebug.ini

/etc/init.d/php"$PHP_VERSION"-fpm start

ln -sf /docker/php/nginx.conf /etc/nginx/nginx.conf
readonly NGINX_CACHE_DIR=/var/cache/nginx
rm -rf "$NGINX_CACHE_DIR" && mkdir -p "$NGINX_CACHE_DIR"

chown -R www-data:www-data "$PWD" "$NGINX_CACHE_DIR"

exec nginx -g 'daemon off;'