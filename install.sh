#!/usr/bin/env bash

cd api
composer install

cd ../
docker-compose up --build -d